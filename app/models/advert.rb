# coding: utf-8
class Advert < ActiveRecord::Base
	has_many :uploads, :dependent => :destroy
	has_many :comments, :dependent => :destroy
	belongs_to :acompany
	belongs_to :amodel
	belongs_to :user
	attr_accessible :acompany_id, :amodel_id, :year, :price, :description,
		 :gear, :region, :contacts, :user_id
	acts_as_commentable
	default_scope order("created_at DESC")

	def self.search_by(params)
		gear = []

		
		# regions = []
		# regions << 0 if params[:region1]
		# regions << 1 if params[:region2]
		# regions << 2 if params[:region3]
		# regions << 3 if params[:region4]
		# regions << 4 if params[:region5]
		# regions << 5 if params[:region6]

		gear << 1 if params[:auto]
		gear << 2 if params[:manual]		

		params[:price1] = 0 if params[:price1].blank?
		params[:price2] = 1000000 if params[:price2].blank?	
		params[:year1] = 0 if params[:year1].blank?
		params[:year2] = 2100 if params[:year2].blank?
		case params[:sort]
		when "1"
			a = Advert.unscoped.order("created_at DESC")	
		when "-1"
			a = Advert.unscoped.order("created_at ASC")
		when "2"
			a = Advert.unscoped.order("price DESC")
		when "-2"
			a = Advert.unscoped.order("price ASC")
		when "3"
			a = Advert.unscoped.order("year DESC")
		when "-3"
			a = Advert.unscoped.order("year ASC")
		else 
			a = Advert.unscoped.order("created_at DESC")
		end
		a = a.where(acompany_id: params[:advert_acompany_id]) if !params[:advert_acompany_id].blank?
		a = a.where(amodel_id: params[:advert][:amodel_id]) if !params[:advert].blank? && !params[:advert][:amodel_id].empty?
		# a = a.where(region: regions) if !regions.empty?
		a = a.where(gear: gear) if !gear.empty?
		a = a.where(year: params[:year1].to_i..params[:year2].to_i)
		a = a.where(price: params[:price1].to_i*1000..params[:price2].to_i*1000)

		return a
	end

end
