class Acompany < ActiveRecord::Base
    attr_accessible :name, :tag
    has_many :adverts, :dependent => :destroy
    has_many :amodels, :dependent => :destroy
end
