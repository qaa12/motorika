class Image < ActiveRecord::Base
  attr_accessible :image
  has_attached_file :image, :styles => {
  :original => {:geometry =>"1000x1000>"},
  :small =>  {:geometry =>"190x152>"},
  :normal =>  {:geometry =>"620x420>"}
}

  default_scope order(:updated_at)
end