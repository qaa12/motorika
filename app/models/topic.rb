class Topic < ActiveRecord::Base
	attr_accessible :title, :body, :tag_list
	belongs_to :user
	acts_as_taggable_on :tags
	default_scope order("created_at DESC")
end
