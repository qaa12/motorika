class Upload < ActiveRecord::Base
  attr_accessible :upload, :advert_id
  has_attached_file :upload, :styles => {
  :original => {:geometry =>"1000x1000>"},
  :small =>  {:geometry =>"190x152>"},
  :normal =>  {:geometry =>"620x420>"}
}
  belongs_to :advert

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
      "name" => read_attribute(:upload_file_name),
      "size" => read_attribute(:upload_file_size),
      "url" => upload.url(:original),
      "small_url" => upload.url(:small),
      "delete_url" => upload_path(self),
      "delete_type" => "DELETE"
    }
  end

end
