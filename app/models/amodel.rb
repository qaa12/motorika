class Amodel < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :acompany
  has_many :adverts, :dependent => :destroy
  attr_accessible :name, :tag, :acompany_id
  
  def to_js
  	{
  		"name" => name,
  		"id" => id
  	}
  end

end
