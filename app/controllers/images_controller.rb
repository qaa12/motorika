class ImagesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :html, :json

  def index
    @images = Image.scoped

    respond_with @images do |format|
      format.json { render :json => json_images(@images), :layout => false }
    end
  end

  def create
    @image = Image.new(params[:image])

    if @image.save
     # render :text => view_context.image_tag(resize_image(@image, 300, 250).url, :alt => '')
      render :text => view_context.image_tag(@image.image.url(:normal), :alt => '')
    else
      render :json => @image.errors
    end
  end

private

  def json_images(images)
    json_images = []

    images.each do |image|
      json_images << {
        :thumb => image.image.url(:small),
        :image => image.image.url(:normal)
      }
    end

    json_images
  end

end