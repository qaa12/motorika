class ApplicationController < ActionController::Base
  protect_from_forgery

  def about
    render "adverts/about"
  end
end
