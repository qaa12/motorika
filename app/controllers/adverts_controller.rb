# coding: utf-8
class AdvertsController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create]
  
  def index
    @adverts = Advert.last(9).reverse
    @topics = Topic.last(9).reverse
  end
  
  def show
    @advert = Advert.find params[:id]
    @comments = @advert.comment_threads
  end
  
  def list
    @adverts = Advert.search_by(params)
    @acompanies = Acompany.all
    if !params[:acompany].nil? && !params[:acompany].empty?
      @amodels = Acompany.find(params[:acompany]).amodels
    else
      @amodels = []
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: [{name: "Все модели", id: ""}]+@amodels.map{|x| x.to_js}}
      format.js {}
    end
  end
  
  def new
    @advert = Advert.new
    @acompanies = Acompany.all
    if !params[:acompany].nil? && !params[:acompany].empty?
      @amodels = Acompany.find(params[:acompany]).amodels
    else
      @amodels = []
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json {render json: @amodels.map{|x| x.to_js}}
    end
  end
  
  def create
    @advert = Advert.new(params[:advert])
    @advert.user = current_user
    respond_to do |format|
      if @advert.save
        format.html{redirect_to uploads_path(@advert.id)}
      else
        render action: "new"
      end
    end
  end  

  def edit
    @advert = Advert.find params[:id]
    if current_user == @advert.user
      @acompanies = Acompany.all
      if !params[:acompany].nil? && !params[:acompany].empty?
        @amodels = Acompany.find(params[:acompany]).amodels
      else
        @amodels = @advert.acompany.amodels
      end
      respond_to do |format|
        format.html # index.html.erb
        format.json {render json: @amodels.map{|x| x.to_js}}
      end
    else
      redirect_to '/'
    end
  end

  def update
    if current_user == @advert.user
      @advert = Advert.find params[:id]
      @advert.update_attributes params[:advert]
      redirect_to @advert
    else
      redirect_to '/' 
    end
  end

  def add_comment
    @advert = Advert.find(params[:id])
    @user_who_commented = current_user
    @comment = Comment.build_from( @advert, @user_who_commented.id, params[:comment] )
    @comment.save
    @comments = @advert.comment_threads
    respond_to do |format|
      format.js
    end
  end
  
end
