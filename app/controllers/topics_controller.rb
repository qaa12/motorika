class TopicsController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create]

  def new
    @topic = Topic.new
    respond_to do |format|
      format.js{}
      format.json{}
      format.html # index.html.erb
    end
  end
  
  def create
    @topic = Topic.new(params[:topic])
    @topic.user = current_user
    respond_to do |format|
      if @topic.save
        format.html{redirect_to @topic}
      else
        render action: "new"
      end
    end
  end  

  def show
  	@topic = Topic.find(params[:id])
  end
  
  def list
    @tags = Topic.tag_counts_on(:tags)
    if params[:tag].blank?
      @topics_last = Topic.paginate(:page => params[:page_last], :per_page => 9)
    else
      @topics_last = Topic.tagged_with(params[:tag]).paginate(:page => params[:page_last], :per_page => 9)
    end
#    @topics_tags = Topic.paginate(:page => params[:page_tags], :per_page => 4)
  end
  
  def profile
    @user = User.find_by_name params[:name]
    @adverts = @user.adverts
    @topics = @user.topics
    #TODO if current_user then edit profile
  end

end
