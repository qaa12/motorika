# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Acompany.create([{name: "Aston Martin", tag: "astonMartin"}, {name: "Audi", tag: "audi"}])
Amodel.create([{name: 'DB9', tag: 'db9', acompany_id: 1}, {name: 'A6', tag: 'a6', acompany_id: 2}, {name: 'A4', tag: 'a4', acompany_id: 2}])
User.create({name: 'test', email: 'test@test.test', password: "111111"})

Advert.create(
{
	description: 'Автомобиль покупался у оф. дилера “Ауди витебский”. Я один хозяин. Вкладывал денег не жалея. Моя машина в идеальнейшем состоянии как внутри, так и внешне. Так трепетно я следил за ее внешним видом, что боковые стекла ни разу не открывал, чтоб не царапались). Состояние лакокрасочного покрытия – идеальное, т.к. после мойки не сушили грязными тряпками кузов и круговыми движениями не матовали краску. Цвет белый металик. Черный потолок. Интересная комплектация. Полный привод. Автомобиль на гарантии 

По птс 211л.с. По факту сейчас 350л.с.
Стоит турбо к04+даунпайп+большой интеркулер охлаждения
Прямой выхлоп (задний диффузор от S4 + выхлоп английской фирмы “MILLTEK”)
реально дорогой и качественный выхлоп.
Передняя решетка от S4 + зеркала от S4
Все устанавливалось у оф. дилера.

Ну и самое интересное – “Etabeta” R19 белые итальянские диски + новая резина 255 35 R19 (см. летнее фото)
Состояние колес идеальное, проехал на них 3 месяца + отдаю в придачу оригинальные R18 + зимняя резина
Все отдаю вместе с машиной. Хочу себе Audi RS5.

Просьба перекупщиков и автосалонов не звонить.',
price: 1400000,
amodel_id: Amodel.find_by_name("A4").id,
acompany_id: Acompany.find_by_name("Audi").id,
user_id: 1,
gear: 1,
year: 2011,
contacts: '+7123456789 Пётр',
}
)

Upload.create(:advert_id => 1, :upload => File.new('db/test/1.jpg', "r"))
Upload.create(:advert_id => 1, :upload => File.new('db/test/2.jpg', "r"))
Upload.create(:advert_id => 1, :upload => File.new('db/test/3.jpg', "r"))
