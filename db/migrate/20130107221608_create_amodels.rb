class CreateAmodels < ActiveRecord::Migration
  def change
    create_table :amodels do |t|
      t.string :name
      t.string :tag, :null => false
      t.integer :acompany_id
      t.timestamps
    end
  end
end
