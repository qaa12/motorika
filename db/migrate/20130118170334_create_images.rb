class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
#      t.string  :image_uid, :null => false
      t.string   "image_file_name"
      t.string   "image_content_type"
      t.integer  "image_file_size"
      t.datetime "image_updated_at"

      t.timestamps
    end

#    add_index :images, :image_uid
  end
end