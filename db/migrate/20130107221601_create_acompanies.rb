class CreateAcompanies < ActiveRecord::Migration
  def self.up
    create_table :acompanies do |t|
      t.string :name
      t.string :tag, :null => false
      t.timestamps
    end
  end

end
