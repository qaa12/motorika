class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.text :description
      t.integer :price
      t.integer :amodel_id  
      t.integer :acompany_id
      t.integer :user_id
      t.integer :gear # 1:автома 2:ручка
      t.integer :year
      t.string :contacts
      # t.integer :region #Астрахань и область: 0, Волгоград и область: 1, Краснодарский край: 2, Республика Адыгея: 3, Республика Калмыкия: 4, Ростов-на-Дону и область: 5
      
      t.timestamps
    end
    add_index :adverts, :price              
    add_index :adverts, :amodel_id
    add_index :adverts, :acompany_id
    add_index :adverts, :user_id
    add_index :adverts, :year
    # add_index :adverts, :region
  end
end
