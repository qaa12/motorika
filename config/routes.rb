Motorika::Application.routes.draw do
  mount ImperaviRails::Engine => "/imperavi"

  devise_for :users
  resources :uploads, :except => [:index, :create]
  match 'addphoto/:id' => 'uploads#index', :as => "uploads", :via => [:get]
  match 'addphoto/:id' => 'uploads#create', :as => "uploads", :via => [:post]
  resources :adverts
  match 'about' => 'application#about'
  match 'alladverts' => 'adverts#list', :as => "alladverts"
  match 'blogs' => 'topics#list', :as => "alltopics"
  match 'add_advert_comments' => 'adverts#add_comment', :as => "add_advert_comments"
  resources :topics
  resources :images, :only => [:index, :create]
  get 'blogs/:tag', to: 'topics#list', as: :tag
  match 'profile/:name' => 'topics#profile', :as => "user"

  root :to => 'adverts#index'

end
